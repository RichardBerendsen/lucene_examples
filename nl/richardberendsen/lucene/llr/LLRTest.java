package nl.richardberendsen.lucene.llr;

import nl.richardberendsen.lucene.llr.LLR;

class LLRTest {

    private LLRTest() {}; // this is a command-line program

    public static void main(String[] args) {

        if (args.length != 0) {
            System.out.println("Usage: java nl.richardberendsen.llr.LLRTest");
            System.exit(2);
        }

        LLR llr; // pointer that gets assigned a new llr object for each test.
       
        System.out.print("Two fair coins, statistic should be zero. G: ");
        /* test data, two  fair coins, statistic should be zero:
         *                                  coin1       coin2
         * coin toss shows heads            50          50
         * sample_sizes                     100         100 
         */
        llr = new LLR("heads", new String[]{"coin1", "coin2"},
                new long[]{50L, 50L}, new long[]{100L, 100L});
        System.out.print(llr.G + "\n");


        System.out.print("Two biased coins, but equally biased, statistic should be zero. G: ");
        /* test data, two  fair coins, statistic should be zero:
         *                                  coin1       coin2
         * coin toss shows heads            50          50
         * sample_sizes                     80          80 
         */
        llr = new LLR("heads", new String[]{"coin1", "coin2"},
                new long[]{50L, 50L}, new long[]{80L, 80L});
        System.out.print(llr.G + "\n");


        System.out.print("One fair coin, one biased, statistic should be 5.0338783876722175\n");
        System.out.print("    (according to my Python handcrafted calculation). G: ");
        /* test data, two  fair coins, statistic should be zero:
         *                                  coin1       coin2
         * coin toss shows heads            50          75 
         * sample_sizes                     100         100 
         */
        llr = new LLR("heads", new String[]{"coin1", "coin2"},
                new long[]{50L, 75L}, new long[]{100L, 100L});
        System.out.print(llr.G + "\n");
    }

}


/*
 * References:
 * Sheskin, D. J. (2011). Handbook of Parametric and Nonparametric Statistical Procedures.
 */


