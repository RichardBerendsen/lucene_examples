package nl.richardberendsen.lucene.llr;

/*
 * Sample salient terms from tweets labeled with particular hashtags using the
 * log-likelihood ratio.
 */

import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.Date;
import java.util.Map;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Comparator;
import java.util.ArrayList;
import java.util.List;
import java.util.regex.Pattern;
import java.util.Collections;

import org.apache.lucene.analysis.Analyzer;
import org.apache.lucene.analysis.standard.StandardAnalyzer;
import org.apache.lucene.document.Document;
import org.apache.lucene.index.DirectoryReader;
import org.apache.lucene.index.IndexReader;
import org.apache.lucene.index.Term;
import org.apache.lucene.index.Terms;
import org.apache.lucene.index.TermsEnum;
import org.apache.lucene.queryparser.classic.QueryParser;
import org.apache.lucene.search.IndexSearcher;
import org.apache.lucene.search.Query;
import org.apache.lucene.search.TermQuery;
import org.apache.lucene.search.ScoreDoc;
import org.apache.lucene.search.TopDocs;
import org.apache.lucene.store.FSDirectory;
import org.apache.lucene.util.BytesRef;
import org.apache.lucene.util.Version;

import nl.richardberendsen.lucene.llr.LLR;

public class Model1 {

    private Model1() {} // this is a command line program.

    /** Generate queries based on LLR */
    public static void main(String[] args) throws Exception {

        boolean minDocFreqGiven = false;
        boolean minExpFreqGiven = false;
        boolean minGGiven = false;
        boolean maxTermsGiven = false;
        long minDocFreq = 0;
        double minExpFreq = 0.0d;
        double minG = 0.0d;
        int maxTerms = 0;
        List<String> posargs = new ArrayList<String>();
        for (int i = 0; i < args.length; ++i) {
            if (args[i].equals("--min-doc-freq")) {
                minDocFreqGiven = true;
                minDocFreq = Long.parseLong(args[++i]);
            } else if (args[i].equals("--min-g")) {
                minGGiven = true;
                minG = Double.parseDouble(args[++i]);
            } else if (args[i].equals("--min-exp-freq")) {
                minExpFreqGiven = true;
                minExpFreq = Double.parseDouble(args[++i]);
            } else if (args[i].equals("--max-terms")) {
                maxTermsGiven = true;
                maxTerms = Integer.parseInt(args[++i]);
            } else
            {
                posargs.add(args[i]);
            }
        }
        for (String arg: posargs) {
            System.out.println("arg: " + arg);
        }

            
        String usage = "Usage:\tjava Model1 index_dir hashtag_tweetid_csv\n"
                + "\n"
                + "Options:\n"
                + "\n"
                + "--min-doc-freq N\n"
                + "\tThe minimal document frequency of query terms\n"
                + "\n"
                + "--min-g\n"
                + "\tThe minimal value of the G statistic for query terms\n"
                + "\n"
                + "--max-terms\n"
                + "\tThe maximal number of query terms for any query\n"
                + "\n"
                + "--min-exp-freq\n"
                + "\tThe minimal expected frequency of a term in the foreground corpus: we don't currently use this in our experiments, because our foreground corpora are so small that expected frequencies are always really small, and requiring a higher expected frequency will only select very general query terms\n"
                + "\n";
        if (posargs.size() != 2) {
            System.err.println(usage);
            System.exit(2);
        }

        String indexPath = posargs.get(0);
        String hashCsvPath = posargs.get(1);

        IndexReader reader = DirectoryReader.open(FSDirectory.open(new File(indexPath)));
        long totalCollectionSize = reader.getSumTotalTermFreq("content");
        System.out.println("numDocs = " + reader.numDocs());
        System.out.println("docFreq 'rihanna' = " + reader.docFreq(new Term("content", "rihanna")));
        System.out.println("maxDoc = " + reader.maxDoc());

        IndexSearcher searcher = new IndexSearcher(reader);

        // Read in hashtag csv
        Map<String, List<String>> hash2tweetId = Model1.readHashtagCsv(hashCsvPath);

        // small helper class hold term frequency and total term frequency
        class TF {
            public long tf;
            public long ttf;
            public TF(long _tf, long _ttf) {
                tf = _tf;
                ttf = _ttf;
            }
        }

        System.out.println("Hashtag"
                + "\t" + "Term"             // the term in question.
                + "\t" + "ObsFreqT_h"       // hashtag corpus.
                + "\t" + "TotalTermFreqT_h" // hashtag corpus.
                + "\t" + "RelFreqT_h"       // hashtag corpus.
                + "\t" + "ExpFreqT_h"       // hashtag corpus.
                + "\t" + "ObsFreqRest"      // rest of the corpus.
                + "\t" + "TotalTermFreqRest"// rest of the corpus.
                + "\t" + "RelFreqRest"      // rest of the corpus.
                + "\t" + "ExpFreqRest"      // rest of the corpus.
                + "\t" + "Representation"
                + "\t" + "LLR"
                + "\t" + "G");
        // for each hashtag h, loop over all tweets that contain h.
        for (Map.Entry<String, List<String>> hash: hash2tweetId.entrySet()) {
            // for each tweet, loop over all terms in it.
            Map<String, TF> termFreqs = new HashMap<String, TF>();
            for (String tweetId: hash.getValue()) {
                // for each term: 
                TopDocs hits = searcher.search(new TermQuery(new Term("id", tweetId)), 1);
                ScoreDoc[] scoreDocs = hits.scoreDocs;
                if (scoreDocs.length == 0) {
                    System.err.println("Notice: "
                            + "tweetId '" + tweetId
                            + "' with hashtag '#" + hash.getKey() 
                            + "' does not appear in the index");
                    continue;
                }
                int docid = scoreDocs[0].doc;
                Terms terms = reader.getTermVector(docid, "content");
                if (null == terms) {
                    continue;
                }
                TermsEnum termsEnum = terms.iterator(null);
                BytesRef binTerm;
                while (null != (binTerm = termsEnum.next())) {
                    String term = binTerm.utf8ToString();

                    //   disregard term if it equals any of the hashtags up to the leading '#' character.
                    if (hash2tweetId.containsKey(term.replaceFirst("^#", ""))) {
                        continue;
                    }

                    //   disregard term if docFreq is lower than minDocFreq
                    if (minDocFreqGiven && reader.docFreq(new Term("content", binTerm)) < minDocFreq) {
                        continue;
                    }

                    //   - get the frequency of the term in the tweet, add this
                    //   to the frequency of the term for h.
                    long tf = termsEnum.totalTermFreq();
                    //   - if you did not see this term before, get the
                    //   frequency of the term in the entire corpus.
                    if (termFreqs.containsKey(term)) {
                        termFreqs.get(term).tf += tf;
                    } else {
                        long ttf = reader.totalTermFreq(new Term("content", binTerm));
                        termFreqs.put(term, new TF(tf, ttf));
                    }
                }
            }

            // Now, with these basic term frequencies, calculate the LLR ratio and
            // other relevant statistics. We already have the total collection size, we still need
            // the collection size for each hashtag.
            long hashCollectionSize = 0;
            for (TF tf: termFreqs.values()) {
                hashCollectionSize += tf.tf;
            }

            // Now we can instantiate (LLR)s, since we have all the info to contain the statistics.
            List<LLR> LLRs = new ArrayList<LLR>();
            for (Map.Entry<String, TF> term2tf: termFreqs.entrySet()) {
                long termFreqT_h = term2tf.getValue().tf;
                long termFreqRest = term2tf.getValue().ttf - termFreqT_h;
                if (termFreqRest == 0) {
                    continue;
                }
                LLRs.add(new LLR(
                            term2tf.getKey(),
                            new String[]{
                                    hash.getKey(),
                                     "rest"},
                            new long[]{
                                    termFreqT_h,
                                    termFreqRest // term frequency in rest corpus.
                            }, 
                            new long[]{
                                    hashCollectionSize,
                                    totalCollectionSize - hashCollectionSize // sum total term frequency rest corpus. 
                            }));
            }

            // Now discard some terms as wanted
            List<Integer> indicesToRemove = new ArrayList<Integer>();
            for (int i=LLRs.size() - 1; i >= 0; --i) {
                // discard term if it is under-represented in the first corpus.
                if (LLRs.get(i).getRelFreq(0) < LLRs.get(i).getRelFreq(1)) {
                    indicesToRemove.add(i);
                    continue;
                }

                // discard term if minG is given, and G < minG
                if (minGGiven) {
                    if (LLRs.get(i).G < minG) {
                        indicesToRemove.add(i);
                        continue; // only need one reason to remove this index.
                    }
                }

                // if minExpFreq is given, delete (in place) LLRs that do
                // not match this criterion
                if (minExpFreqGiven) {
                    boolean removeThisIndex = false;
                    for (int j=0; j < LLRs.get(i).k; j++) {
                        if (LLRs.get(i).getExpFreq(j) < minExpFreq) {
                            removeThisIndex = true;
                            break;
                        }
                    }
                    if (removeThisIndex) {
                        indicesToRemove.add(i);
                        continue;
                    }
                }
            }
            // walk through indices in reverse order, so as to be able to remove them,
            // without the content changing due to shifting.
            for (int indexToRemove: indicesToRemove) {
                LLRs.remove(indexToRemove);
            }

            // Now we sort terms by LLR, and print out if terms are over or under-represented.
            Collections.sort(LLRs, Collections.reverseOrder(new LLRComparator())); // sort LLRs in place, descending
            
            // loop over terms in LLRs, and print out statistics
            int offsetEnd;
            if (maxTermsGiven) {
                offsetEnd = Math.min(maxTerms, LLRs.size());
            }
            else {
                offsetEnd = LLRs.size();
            }
            for (LLR llr: LLRs.subList(0, offsetEnd)) {
                String representation; 
                if (llr.getRelFreq(0) > llr.getRelFreq(1)) {
                    representation = "over-represented";
                } else if (llr.getRelFreq(0) < llr.getRelFreq(1)) {
                    representation = "under-represented";
                } else {
                    representation = "equal";
                }
                // Note, we add the # in the externel representation of the hashtag
                System.out.println("#" + hash.getKey()
                        + "\t" + llr.succesEvent    // the term in question.
                        + "\t" + llr.obsFreqs[0]    // hashtag corpus.
                        + "\t" + llr.sampleSizes[0] // hashtag corpus.
                        + "\t" + llr.getRelFreq(0)  // hashtag corpus.
                        + "\t" + llr.getExpFreq(0)  // hashtag corpus.
                        + "\t" + llr.obsFreqs[1]    // rest of the corpus.
                        + "\t" + llr.sampleSizes[1] // rest of the corpus.
                        + "\t" + llr.getRelFreq(1)  // rest of the corpus.
                        + "\t" + llr.getExpFreq(1)  // rest of the corpus.
                        + "\t" + representation
                        + "\t" + llr.LLR
                        + "\t" + llr.G);
            }
        }

        reader.close();
    }

    /**
     * Reads the hashtag;tweetid csv file
     * Note that the leading '#' is removed, if any is present.
     */
    public static Map<String, List<String>> readHashtagCsv(String path) throws IOException {

        Map<String, List<String>> hash2tweetId = new HashMap<String, List<String>>();

        final File csv = new File(path);
        if (!csv.exists() || !csv.isFile() || !csv.canRead()) {
            throw new IOException("Hashtags csv '"
                    + path
                    + "'\n does not exist, is not a normal file,"
                    + "  or is not readable");
        }
        FileInputStream fis = new FileInputStream(csv);
        BufferedReader buf = new BufferedReader(new
                InputStreamReader(fis, "UTF-8")); 
        Pattern p = Pattern.compile(";");
        String line;
        while (null != (line = buf.readLine())) {
           String[] fields = p.split(line, 2); 
           // remove leading # character in internal representation
           String hashtag = fields[0].replaceFirst("^#", "");
           String tweetId = fields[1]; 
           List<String> tweetIds;
           if (null != (tweetIds = hash2tweetId.get(hashtag))) {
               tweetIds.add(tweetId);
           } else {
               tweetIds = new ArrayList<String>();
               tweetIds.add(tweetId);
               hash2tweetId.put(hashtag, tweetIds);
           }
        }

        return hash2tweetId;

    }
}

class LLRComparator implements Comparator<LLR> {
    @Override
    public int compare(LLR llr1, LLR llr2) {
        if (llr1.LLR < llr2.LLR) {
            return -1;
        } else if (llr1.LLR > llr2.LLR) {
                return 1;
        } else { // llr1.LLR == llr2.LLR
            return 0;
        }
    }
}
