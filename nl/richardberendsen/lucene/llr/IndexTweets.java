package nl.richardberendsen.lucene.llr;

/*
 * adapted from Lucene Demo
 */

import org.apache.lucene.analysis.Analyzer;
import org.apache.lucene.analysis.standard.StandardAnalyzer;
import org.apache.lucene.document.Document;
import org.apache.lucene.document.Field;
import org.apache.lucene.document.FieldType;
import org.apache.lucene.document.StringField;
import org.apache.lucene.index.IndexWriter;
import org.apache.lucene.index.IndexWriterConfig.OpenMode;
import org.apache.lucene.index.IndexWriterConfig;
import org.apache.lucene.index.Term;
import org.apache.lucene.store.Directory;
import org.apache.lucene.store.FSDirectory;
import org.apache.lucene.util.Version;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.Date;
import java.util.Vector;
import java.util.regex.Pattern;

/** Index a collection csv file.
 * <p>
 * This is a command line application that indexes a collection csv file, and
 * writes an index to a given directory.
 */
public class IndexTweets {

    private IndexTweets() {}

    public static void main(String[] args) {

        Vector<String> posargs = new Vector<String>();
        float max_heap_mb = 256.0f;

        for (int i = 0; i < args.length; ++i) {
            System.out.println("arg " + i + ": " + args[i]);
            if (args[i].equals("--max-heap-mb")) {
                max_heap_mb = Float.parseFloat(args[++i]);
                System.out.println("max_heap_mb: " + max_heap_mb);
            } else {
                posargs.add(args[i]);
            }
        }

        String usage = "java IndexTweets index_dir collection_csv [--max-heap-mb X]";
        if (posargs.size() != 2) {
            System.err.println("Usage: " + usage);
            System.exit(2);
        }

        String indexPath = posargs.get(0);
        String csvPath = posargs.get(1);

        final File csv = new File(csvPath);
        if (!csv.exists() || !csv.isFile() || !csv.canRead()) {
            System.err.println("collection_csv '"
                    + csvPath
                    + "'\n does not exist, is not a normal file,"
                    + "  or is not readable");
            System.exit(1);
        }

        final File indexFile = new File(indexPath);
        if (indexFile.exists()) {
            System.err.println("index_dir already exists");
            System.exit(1);
        }

        Date start = new Date();
        try {

            System.out.println("Indexing to directory: '"
                    + indexPath + "' ...");

            Directory dir = FSDirectory.open(indexFile);
            /*
             * From: http://lucene.apache.org/core/4_6_0/demo/overview-summary.html#overview_description:
             * The Analyzer we are using is StandardAnalyzer, which creates
             * tokens using the Word Break rules from the Unicode Text
             * Segmentation algorithm specified in Unicode Standard Annex #29;
             * converts tokens to lowercase; and then filters out stopwords.
             * Stopwords are common language words such as articles (a, an,
             * the, etc.) and other tokens that may have less value for
             * searching
             */
            Analyzer analyzer = new StandardAnalyzer(Version.LUCENE_46);
            IndexWriterConfig iwc = new IndexWriterConfig(Version.LUCENE_46, analyzer);
            iwc.setOpenMode(OpenMode.CREATE);
            System.out.println("codec: " + iwc.getCodec());

             // Optional: for better indexing performance, if you
             // are indexing many documents, increase the RAM
             // buffer.  But if you do this, increase the max heap
             // size to the JVM (eg add -Xmx512m or -Xmx1g):
             // 
            iwc.setRAMBufferSizeMB(max_heap_mb);


            IndexWriter writer = new IndexWriter(dir, iwc);
            index(writer, csv);

            // NOTE: if you want to maximize search performance,
            // you can optionally call forceMerge here.  This can be
            // a terribly costly operation, so generally it's only
            // worth it when your index is relatively static (ie
            // you're done adding documents to it):
            //
            // writer.forceMerge(1);

            writer.close();
            
            Date end = new Date();
            System.out.println("...Done, after "
                    + (end.getTime() - start.getTime())
                    + " milliseconds.");

        } catch (IOException e) {
            System.out.println(" caught a "
                    + e.getClass()
                    + "\n with message: "
                    + e.getMessage());
        }

    }


    /**
     * Indexes the given file using the given writer 
     * 
     * @param writer Writer to the index where the given file/dir info will be stored
     * @param file The file to index
     * @throws IOException If there is a low-level I/O error
     */
    static void index(IndexWriter writer, File file)
            throws IOException {
        // do not try to index files that cannot be read
        if (file.canRead()) {
            FileInputStream fis;
            try {
              fis = new FileInputStream(file);
            } catch (FileNotFoundException fnfe) {
                // at least on windows, some temporary files raise this exception with an "access
                // denied" message checking if the file can be read doesn't help
                return;
            }
      
            // First create the field type we want to use for the tweet text.
            // Because we want to store term vectors for each document, we
            // can't use one of the convenience classes, like TextField.
            FieldType textType = new FieldType();
            textType.setIndexed(true);
            textType.setStored(false); // actually this is the default, but I add it for clarity.
            textType.setTokenized(true); // actually this is the default, but I add it for clarity.
            textType.setStoreTermVectors(true);
            try {
      
                BufferedReader buf = new BufferedReader(new InputStreamReader(fis, "UTF-8"));
                Pattern p = Pattern.compile(";");
                String line;
                while (null != (line = buf.readLine())) {

                    // split line
                    // example line:
                    // 10257182;1296570859.0;and now is back up
                    // note that the text may contain a ;, for all I know.
                    String[] fields = p.split(line, 3);
                    String tweetId = fields[0];
                    String timestamp = fields[1];
                    String text = fields[2];

                    // make a new, empty document
                    Document doc = new Document();
          
                    // Add the id of the tweet as a field named "id".  Use a
                    // field that is indexed (i.e. searchable), but don't tokenize 
                    // the field into separate words and don't index term frequency
                    // or positional information:
                    Field idField = new StringField("id", tweetId, Field.Store.YES);
                    doc.add(idField);
          
                    // Add the contents of the file to a field named "contents".
                    //
                    doc.add(new Field("content", text, textType));
          
                    // We are using a new index, so we just add the document
                    // (no old document can be there):
                    writer.addDocument(doc);
                }
            } 
            finally {
                fis.close();
            }
        }
    }
}
