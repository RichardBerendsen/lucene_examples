package nl.richardberendsen.lucene.llr;

import java.util.Arrays;

/* 
 * Compute LLR ratio for a one-dimensional table. This class wants for each cell the
 * observed frequency and sample size. 
 *
 * This is suitable for the interpretation that we sample subjects, assign them
 * to a cell based on some characteristic, and perform a Bernouilli experiment
 * on them.
 *
 * If the characteristic has no bearing on the succes chances of the Bernouilli 
 * experiment, we would expect that the observed successes in each cell match the
 * expected successes in each cell.
 * */
public class LLR {
    
    // pointers to objects, used read-only, but exposed for convenience. Note
    // that if you change one of the objects these pointers refer to, objects
    // of this class will be affected by this change.
    public final String succesEvent;
    public final String[] groupNames;
    public final long[] obsFreqs;
    public final long[] sampleSizes;

    // computed, public
    public double LLR;
    public double G;

    // computed, private (but exposed through getter)
    private double[] _relFreqs; // nice to have to see in which direction differences are.
    private double[] _expFreqs; // useful for checking conditions of applying statistical test.

    // computed, public, for internal use.
    public final int k;

    /*
     * LLR(...) is the sole constructor.
     */
    public LLR(
            String _succesEvent,
            String[] _groupNames,
            long[] _obsFreqs,
            long[] _sampleSizes
            ) {
        // swallow references
        succesEvent = _succesEvent;
        groupNames = _groupNames;
        obsFreqs = _obsFreqs;
        sampleSizes = _sampleSizes;

        // check arguments
        k = obsFreqs.length;
        assert k == sampleSizes.length;
        assert k == groupNames.length;

        // compute LLR
        compute();
    }

    private void compute() {
        long totObsFreq = 0L;
        for (long x: obsFreqs) {
            totObsFreq += x;
        }

        long totSampleSize = 0L;
        for (long x: sampleSizes) {
            totSampleSize += x;
        }

        _expFreqs = new double[k];
        _relFreqs = new double[k];

        double _LLR = 0.0d;
        for (int i = 0; i < k; ++i) {
            _expFreqs[i] = (sampleSizes[i] * totObsFreq)
                    / (double) totSampleSize;
            _relFreqs[i] = obsFreqs[i] / (double)sampleSizes[i];
            _LLR += obsFreqs[i] * Math.log(obsFreqs[i] / _expFreqs[i]);
        }
        LLR = _LLR;
        G = 2 * LLR;
    }

    public double[] getRelFreqs() {
        return Arrays.copyOf(_relFreqs, _relFreqs.length);
    }

    public double getRelFreq(int i) {
        return _relFreqs[i];
    }

    public double[] getExpFreqs() {
        return Arrays.copyOf(_expFreqs, _expFreqs.length);
    }

    public double getExpFreq(int i) {
        return _expFreqs[i];
    }
}
