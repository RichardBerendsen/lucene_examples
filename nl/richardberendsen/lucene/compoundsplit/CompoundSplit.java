package nl.richardberendsen.lucene.compoundsplit;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.PrintStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.Date;
import java.util.Map;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Comparator;
import java.util.List;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.regex.Pattern;
import java.util.Collections;

import org.apache.lucene.analysis.Analyzer;
import org.apache.lucene.analysis.standard.StandardAnalyzer;
import org.apache.lucene.document.Document;
import org.apache.lucene.index.DirectoryReader;
import org.apache.lucene.index.IndexReader;
import org.apache.lucene.index.MultiFields;
import org.apache.lucene.index.Term;
import org.apache.lucene.index.Terms;
import org.apache.lucene.index.TermsEnum;
import org.apache.lucene.queryparser.classic.QueryParser;
import org.apache.lucene.search.IndexSearcher;
import org.apache.lucene.search.Query;
import org.apache.lucene.search.TermQuery;
import org.apache.lucene.search.ScoreDoc;
import org.apache.lucene.search.TopDocs;
import org.apache.lucene.store.FSDirectory;
import org.apache.lucene.util.BytesRef;
import org.apache.lucene.util.Version;

import nl.richardberendsen.math.DescriptiveStats;

/**
 * Do compound splitting using collection frequencies obtained from an index.
 *
 * Todo:
 *  - minimum length should be one, but perhaps only including pre-defined words
 *  - 
 */
class CompoundSplit {

    private CompoundSplit(){}

    public static void main(String[] args) throws IOException {

        String sortBy = "GM";
        int minLengthCompound = 1;
        int maxLengthToSplit = 23; // big strings will consume a lot of memory, 23
                                   // happens to be the length of the second
                                   // largest string I was interested in
                                   // splitting, and this one I could do with
                                   // just 2G of memory.
        boolean verbose = false;
        List<String> posargs = new ArrayList<String>();
        for (int i = 0; i < args.length; ++i) {
            if (args[i].equals("--min-length-compound")) {
                minLengthCompound = Integer.parseInt(args[++i]);
                if (minLengthCompound < 1) {
                    throw new IllegalArgumentException(
                            "--min-length-compound should be greater than one");
                }
            } else if (args[i].equals("--max-length-to-split")) {
                maxLengthToSplit = Integer.parseInt(args[++i]);
                if (maxLengthToSplit < 1) {
                    throw new IllegalArgumentException(
                            "--max-length-to-split should be greater than one");
                }
            } else if (args[i].equals("--sort-by")) {
                sortBy = args[++i];
                if (!(sortBy.equals("GM") || sortBy.equals("minFreq"))) {
                    throw new IllegalArgumentException(
                            "--sort-by should be either GM or minFreq");
                }
            } else if (args[i].equals("--verbose") || args[i].equals("-v")) {
                verbose = true;
            } else {
                posargs.add(args[i]);
            }
        }
            
        String usage = "Usage:\tjava CompoundSplit index_dir strings.csv\n"
                + "\n" 
                + "Options: \n" 
                + "\t--min-length-compound X (default: 1)\n"
                + "\t--max-length-to-split X (default: 23)\n"
                + "\t--sort-by [GM|minFreq] (default: GM)\n";
        if (posargs.size() != 2) {
            System.err.print(usage);
            System.exit(2);
        }

        String indexPath = posargs.get(0);
        String stringsCsvPath = posargs.get(1);

        // buildLexicon may throw IOException, main specifies, so
        // we don't have to catch.
        HashMap<String, Long> lexicon = buildLexicon(indexPath,
                minLengthCompound);

        // read Strings to split (throws FileNotFoundException, which main
        // specifies)
        List<String> strings = readStrings(stringsCsvPath);

        // output for each string all candidate splits
        splitStrings(strings, lexicon, System.out, minLengthCompound,
        maxLengthToSplit, sortBy, verbose);
    }


    public static HashMap<String, Long> buildLexicon(String indexPath,
            int minLengthCompound) throws IOException {
        HashMap<String, Long> lexicon = new HashMap<String, Long>();

        // enumerate over Terms in Index, build lexicon.
        IndexReader reader = DirectoryReader.open(FSDirectory.open(new File(indexPath)));
                // may throw IOException, which we specify.

        // log some basic counts
        System.err.println("Feelgood: Number of docs in index: "
                + reader.numDocs());

        // get terms
        Terms terms = MultiFields.getTerms(reader, "content");
        if (null == terms) {
            return lexicon;
        }
        TermsEnum termsEnum = terms.iterator(null);
        BytesRef bytesRef;
        while (null != (bytesRef = termsEnum.next())) { // may throw IOException
            String string =  bytesRef.utf8ToString();
            if (string.length() >= minLengthCompound) {
                Term term = new Term("content", bytesRef);
                long freq  = termsEnum.totalTermFreq();
                lexicon.put(string, freq);
            }
        }

        return lexicon;
    }

    /**
     * reads strings from file. Supply one string per line.
     */
    public static List<String> readStrings(String path) throws IOException {
        File f = new File(path);
        // may throw FileNotFoundException, which we specify:
        FileInputStream fis = new FileInputStream(f);
        BufferedReader buf = new BufferedReader(new InputStreamReader(fis, "UTF-8")); 
        List<String> strings = new ArrayList<String>();
        String line;
        // may throw IOException, which we specify:
        while (null != (line = buf.readLine())) {
            strings.add(line.trim());
        }
        return strings;
    }

    public static void splitStrings(
            List<String> strings, // used read-only
            HashMap<String, Long> lexicon, // used read-only
            PrintStream out, // printed to (duh)
            int minLengthCompound,
            int maxLengthToSplit,
            String sortBy,
            boolean verbose) {

        // local class to store term with collection frequency.
        class LexiconTerm {
            public final String term; // not modified by LexiconTerm
            public final long frequency; // not modified by LexiconTerm
            public LexiconTerm(String s, long l) {
                term = s;
                frequency = l;
            }
        }

        // local class to store split together with collection frequencies and
        // geometric mean. Sortable using comparator below.
        class SplitGM {
            public List<LexiconTerm> split =
                    new ArrayList<LexiconTerm>(); // owned by SplitGM
            public final double GM; // geometric mean
            public final long minFreq;
            private int[] _frequencies;

             // @param lexicon is used read-only by the constructor.
            public SplitGM(List<String> _split,
                    HashMap<String, Long> lexicon
                    ) {
                long[] x = new long[_split.size()];
                for (int i = 0; i < _split.size(); ++i) {
                    String s = _split.get(i);
                    long freq = lexicon.get(s);
                    split.add(new LexiconTerm(s, freq));
                    x[i] = freq;
                }
                GM = DescriptiveStats.geometricMean(x);
                minFreq = DescriptiveStats.min(x);
            }

            @Override
            public String toString() {
                String s = "GM:" + GM + "\tminFreq:" + minFreq;
                for (LexiconTerm lt: split) {
                    s += "\t" + lt.term + ":" + lt.frequency;
                }
                return s + "\n";
            }
        }

        class SplitGMbyGM implements Comparator<SplitGM> {
            @Override
            public int compare(SplitGM l, SplitGM r) {
                if (l.GM < r.GM) {
                    return -1;
                } else if (l.GM > r.GM) {
                    return 1;
                } else {
                    return 0;
                }
            }
        }

        class SplitGMbyMinFreq implements Comparator<SplitGM> {
            @Override
            public int compare(SplitGM l, SplitGM r) {
                if (l.minFreq < r.minFreq) {
                    return -1;
                } else if (l.minFreq > r.minFreq) {
                    return 1;
                } else {
                    return 0;
                }
            }
        }

        for (String s: strings) {
            // do not split overly long words to save resources
            if (s.length() > maxLengthToSplit) {
                List<String> splt = new ArrayList<String>(Arrays.asList(s));
                SplitGM splitGM = new SplitGM(splt, lexicon);
                if (verbose) {
                    out.print(s + "\t");
                    out.print(splitGM);
                } else {
                    out.print(s);
                    for (String compound: splt) {
                        out.print("\t" + compound);
                    }
                    out.print("\n");
                }
                continue;
            }

            // get candidate splits
            List<List<String>> splits = 
                    new ArrayList<List<String>>();
            split(s, new ArrayList<String>(), lexicon, 
                    minLengthCompound, splits);

            if (splits.size() == 0) {
                System.err.print("Notice: '"
                        + s + "'has no splits");
                if (lexicon.containsKey(s)) {
                    System.err.print(", and is in lexicon\n");
                } else {
                    System.err.print(", and is not in lexicon\n");
                }
                out.println(s + "\t" + s);
            }

            // calculate geometric mean and minFreq for each split
            List<SplitGM> splitsGM = new ArrayList<SplitGM>();
            for (List<String> split: splits) {
                splitsGM.add(new SplitGM(split, lexicon));
            }
            
            // sort
            if (sortBy.equals("GM")) {
                Collections.sort(splitsGM, new SplitGMbyGM());
            } else {
                Collections.sort(splitsGM, new SplitGMbyMinFreq());
            }

            // and output
            if (verbose) {
                // output them all
                for (SplitGM splitGM: splitsGM) {
                    out.print(s + "\t");
                    out.print(splitGM);
                }
            } else {
                // output only the last one
                if (splitsGM.size() > 0) {
                    SplitGM splitGM = splitsGM.get(splitsGM.size() - 1);
                    out.print(s);
                    for (LexiconTerm lt: splitGM.split) {
                        out.print("\t" + lt.term);
                    }
                    out.print("\n");
                }
            }
        }
    }

    /**
     * contains the juice. This small function performs the compound splitting, the rest
     * is glue code.
     *
     * It is quite simple, the only part that is a bit involved is that split will not
     * split consecutive numbers (It will treat them as a single number token).
     *
     * s should be alphanumeric for best results.
     */
    public static void split(String s, // read-only for split
            List<String> prefixes ,
                    // read-only for split (in particular, split will make a shallow copy of prefixes, 
                    // passing on the pointers to the String objects in prefixes to itself (in a 
                    // recursive call). These String objects are used read-only. In the end,
                    // pointers to these same String objects end up in splits. Therefore, it should
                    // be understood that when you use splits yourself later on, the String objects
                    // in it should be used read-only.
            HashMap<String, Long> lexicon, // read-only 
            int minLengthCompound,
            List<List<String>> splits // modified by split 
        ) {
        for (int end = minLengthCompound; end <= s.length(); ++end) {
            String prefix = s.substring(0, end);

            // if last character of prefix is a digit, and the next character
            // (if any) is a digit, continue (which will eat the next character
            // as well).
            if (end < s.length() && prefix.matches("[0-9]$") && 
                    s.substring(end, s.length()).matches("^[0-9]")) {
                continue;
            }

            if (lexicon.containsKey(prefix)) {
                List<String> prefixesShallowCopy = new ArrayList<String>(prefixes);
                prefixesShallowCopy.add(prefix);
                if (end == s.length()) {
                    splits.add(prefixesShallowCopy); // we have a split
                } else {
                    String tail = s.substring(end, s.length());
                    split(tail, prefixesShallowCopy, lexicon,
                           minLengthCompound, splits);
                }
            }
        }
    }
}
