package nl.richardberendsen.lucene.querygeneration;

import java.util.Random;

import nl.richardberendsen.lucene.querygeneration.LM;

public class Uniform implements LM {

    String[] vocabulary;

    @Override
    public String sample(Random random) {
        // random.nextInt(x) samples from [0, x)
        return vocabulary[random.nextInt(vocabulary.length)];
    }

    public Uniform(String[] vocabulary) {
        this.vocabulary = vocabulary;
    }
}
