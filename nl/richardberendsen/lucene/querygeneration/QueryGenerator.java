package nl.richardberendsen.lucene.querygeneration;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.PrintStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.Date;
import java.util.Map;
import java.util.HashMap;
import java.util.Set;
import java.util.HashSet;
import java.util.SortedMap;
import java.util.TreeMap;
import java.util.Comparator;
import java.util.List;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.regex.Pattern;
import java.util.Collections;
import java.util.Random;

import org.apache.lucene.analysis.Analyzer;
import org.apache.lucene.analysis.standard.StandardAnalyzer;
import org.apache.lucene.document.Document;
import org.apache.lucene.index.DirectoryReader;
import org.apache.lucene.index.IndexReader;
import org.apache.lucene.index.MultiFields;
import org.apache.lucene.index.Term;
import org.apache.lucene.index.Terms;
import org.apache.lucene.index.TermsEnum;
import org.apache.lucene.queryparser.classic.QueryParser;
import org.apache.lucene.search.IndexSearcher;
import org.apache.lucene.search.Query;
import org.apache.lucene.search.TermQuery;
import org.apache.lucene.search.ScoreDoc;
import org.apache.lucene.search.TopDocs;
import org.apache.lucene.store.FSDirectory;
import org.apache.lucene.util.BytesRef;
import org.apache.lucene.util.Version;

import nl.richardberendsen.math.Interval;

import org.apache.commons.math3.random.RandomDataGenerator;


public class QueryGenerator {


    private QueryGenerator(){}

    public static void main(String[] args) throws IOException {

        Set<String> methods = new HashSet<String>(
                Arrays.asList("popular", "random", "discriminative"));
        String method = "discriminative"; // or: popular, or random
        boolean verbose = false;
        int minDocFreq = 1;
        
        List<String> posargs = new ArrayList<String>();
        for (int i = 0; i < args.length; ++i) {
            if (args[i].equals("--method")) {
                method = args[++i];
                if (!methods.contains(method)) {
                    throw new IllegalArgumentException(
                            "--method should be 'popular', 'random' or 'discriminative'");
                }
            } else if (args[i].equals("--verbose") || args[i].equals("-v")) {
                verbose = true;
            } else if (args[i].equals("--min-doc-freq")) {
                minDocFreq = Integer.parseInt(args[++i]);
                if (minDocFreq < 1) {
                    throw new IllegalArgumentException("--min-doc-freq should be >= 1");
                }
            } else {
                posargs.add(args[i]);
            }
        }
            
        String usage = "Usage:\tjava QueryGenerator index_dir group_docid.csv "
                + "average_query_length seedQueries seedTerms lambda\n"
                + "\n" 
                + "Options: \n" 
                + "\t--method [popular | random | discriminative (default)]"
                + "\t--verbose"
                + "\t--min-doc-freq"
                + "\n"
                + "Implements query generation methods from [1]. Note that it allows\n"
                + "query generation from a group of documents. If each group consists\n"
                + "of just one document the algorithm reduces to the one in [1]\n"
                + "\n"
                + "index_dir is assumed to hold a Lucene index\n"
                + "group_docid.csv has tab separated lines with groupid and docid\n"
                + "\n"
                + "References:\n"
                + "\n"
                + "[1]\n"
                + "Azzopardi, L., De Rijke, M., & Balog, K. (2007, July). Building simulated\n"
                + "queries for known-item topics: an analysis using six european languages. In\n"
                + "Proceedings of the 30th annual international ACM SIGIR conference on Research\n"
                + "and development in information retrieval (pp. 455-462). ACM\n"
                + "\n"
                + "Notes:\n"
                + "\n"
                + "[1] In the original paper it was not mentioned whether sampling with or\n"
                + "without replacement was used; here, we implement sampling with replacement\n";
        if (posargs.size() != 6) {
            System.err.print(usage);
            System.exit(2);
        }

        String indexPath = posargs.get(0);
        String groupDocPath = posargs.get(1);
        double avgQueryLength = Double.parseDouble(posargs.get(2));
        long seedQueries = Long.parseLong(posargs.get(3));
        long seedTerms = Long.parseLong(posargs.get(4));
        double lambda = Double.parseDouble(posargs.get(5));

        if (lambda < 0.0d || lambda > 1.0d) {
            throw new IllegalArgumentException("lambda must be in [0, 1]");
        }

        // create QGConf object, to be able to pass around a single thing between
        // helper functions instead of a host of parameters.
        QGConf qGConf = new QGConf(method, avgQueryLength, verbose, indexPath,
                System.out, seedQueries, seedTerms, lambda, minDocFreq);

        // read groups from groupDocPath
        Map<String, List<String>> groups = readGroups(groupDocPath);

        // open index reader
        // enumerate over Terms in Index, build lexicon.
        IndexReader reader = DirectoryReader.open(FSDirectory.open(new File(indexPath)));
                // may throw IOException, which we specify.
        
        // open index searcher
        IndexSearcher searcher = new IndexSearcher(reader);

        // get sum total term frequency (corpus size)
        long sttf = reader.getSumTotalTermFreq("content");
        
        // build lexicon to calculate p(t_i), and build background language model, in one go.
        // Note, max_likelihood_lm will contain pointers to same strings that are in lexicon
        // Note 2: just try to allocate memory (basically, whole corpus)
        Map<String, Long> lexicon = new HashMap<String, Long>();
        buildLexicon(reader, lexicon);
    
        // just to be sure, check that the sum Lucene gives after adding term
        // frequencies for all terms equals the direct sum it offers.
        long sttf2 = 0L;
        for (Long ttf: lexicon.values()) {
            sttf2 += ttf;
        }
        assert(sttf == sttf2); // if this fails, don't use sttf, use sttf2

        // build corpus language model
        LM corpusLM = new ML(lexicon, sttf);

        // output for each string all candidate splits
        // Note, generateQueries throws IOException
        generateQueries(groups, qGConf, lexicon, corpusLM, reader, searcher);
    }


    /**
     * reads groups from file. Supply lines like this: 'groupId;docId'. 
     */
    public static Map<String, List<String>> readGroups(String path) throws IOException {
        File f = new File(path);
        // may throw FileNotFoundException, which we specify:
        FileInputStream fis = new FileInputStream(f);
        BufferedReader buf = new BufferedReader(new InputStreamReader(fis, "UTF-8")); 
        Map<String, List<String>> groups = new HashMap<String, List<String>>();
        String line;
        // may throw IOException, which we specify:
        while (null != (line = buf.readLine())) {
            if (!line.trim().equals(line)) {
                throw new RuntimeException("encountered line with leading or trailing whitespace");
            } 
            String[] fields = line.split(";");
            if (fields.length != 2) {
                throw new RuntimeException("encountered line with not exactly two fields");
            }
            // remove leading hashtag in internal representation, if it is there.
            String groupId =  fields[0].trim().replaceFirst("^#", "");
            String docId = fields[1].trim();
            if (groups.containsKey(groupId)) {
                groups.get(groupId).add(docId);
            } else {
                groups.put(groupId, new ArrayList<String>(Arrays.asList(docId)));
            }
        }
        return groups;
    }

    public static void buildLexicon(
            IndexReader reader,
            Map<String, Long> lexicon // modified (elements added)
            )
            throws IOException {

        // log some basic counts
        System.err.println("LOG: Number of docs in index: "
                + reader.numDocs());

        // get terms
        Terms terms = MultiFields.getTerms(reader, "content");
        if (null == terms) {
            return;
        }
        TermsEnum termsEnum = terms.iterator(null);
        BytesRef bytesRef;
        while (null != (bytesRef = termsEnum.next())) { // may throw IOException
            String string =  bytesRef.utf8ToString(); // allocates new String
            Term term = new Term("content", bytesRef);
            long freq  = termsEnum.totalTermFreq();
            lexicon.put(string, freq);
        }
    }

    private static void generateQueries(
            Map<String, List<String>> groups, // used read-only
            QGConf qGConf, // used read-only
            Map<String, Long> lexicon, // used read-only
            LM corpusLM, // used read-only
            IndexReader reader,
            IndexSearcher searcher
            ) throws IOException {

        // Random data generator for query lengths (seed it explicitly, to be
        // able to reproduce)
        RandomDataGenerator rdg = new RandomDataGenerator();
        rdg.reSeed(qGConf.seedQueries);

        // Random data generator for sampling terms (seed it explicitly, too)
        Random random = new Random(qGConf.seedTerms);

        for (Map.Entry<String, List<String>> entry: groups.entrySet()) {
            // Note, generateQuery throws IOException
            generateQuery(entry.getKey(), entry.getValue(), groups, reader,
                    searcher, rdg, random, qGConf, lexicon, 
                    corpusLM);
        }
    }

    /**
     * Generates a query using method from [1]. The comments in the code
     * come from that paper.
     * 
     * References:
     *
     * [1]
     * Azzopardi, L., De Rijke, M., & Balog, K. (2007, July). Building simulated
     * queries for known-item topics: an analysis using six european languages. In
     * Proceedings of the 30th annual international ACM SIGIR conference on Research
     * and development in information retrieval (pp. 455-462). ACM
     */
    private static void generateQuery(
            String groupId, // used read-only
            List<String> docIds, // used read-only
            Map<String, List<String>> groups, // used read-only
            IndexReader reader, // used read-only
            IndexSearcher searcher, // used read-only
            RandomDataGenerator rdg, // used read-only, for query length sampling
            Random random, // used read-only
            QGConf qGConf, // used read-only
            Map<String, Long> lexicon, // used read-only
            LM corpusLM // used read-only
            ) throws IOException {
        // Azzopardi-step (1) initialize empty query
        // Azzopardi-step (2) select document(s) to sample terms from, in this implementation already
        // assumed given.
        // Azzopardi-step (3) select the query length s with probability p(s) 
        int queryLength = sampleQueryLength(qGConf.avgQueryLength, rdg);
        qGConf.printVerbose("==========\n");
        qGConf.printVerbose("query for groupId '" + groupId + "'\n");
        qGConf.printVerbose("queryLength: " + queryLength + "\n");

        if (queryLength > 0) {
            List<String> query = new ArrayList<String>();

            // Instantiate language model for P(t_i|docIds) here.
            LM groupLM = getLM(groupId, docIds, reader, searcher, lexicon, qGConf);
            // Azzopardi-step (4) repeat s times:
            while (query.size() < queryLength) {
                // Azzopardi-step (4a) sample a term t_i from the document model of the concatenation
                // of documents in docIds, according to method.
                // Azzopardi-step (4b) add t_i to the query
                //
                // Note, sampleTerm throws IOException
                String term = sampleTerm(corpusLM, groupLM, random, groups, reader, qGConf);
                if (term == "") {
                    break; // apparently there were no good terms to sample anymore.
                } else {
                    query.add(term);
                }
            }

            // Azzopardi-step (5) Record docIds and q to define the
            // relevant-docs/query pair. (i.e. print query, the rel_docs were
            // input for this implementation, you'll remember them yourself)
            //
            // Note, we add the # in the representation on disk (the encoded
            // representation)
            System.out.print("#" + groupId);
            for (String term: query) {
                System.out.print("\t" + term);
            }
            System.out.print("\n");
        }
        qGConf.printVerbose("==========\n");
    }


    private static LM getLM(
                String groupId,
                List<String> docIds,
                IndexReader reader,
                IndexSearcher searcher, 
                Map<String, Long> lexicon,
                QGConf qGConf
    ) throws IOException {
        // Calculate term frequencies for terms in group
        Map<String, Long> termFreqsGroup = new HashMap<String, Long>();
        getTermFreqsGroup(groupId, docIds, reader, searcher, termFreqsGroup);
        long sttf_group = 0;
        for (Long x: termFreqsGroup.values()) {
            sttf_group += x;
        }

        // calculate P(t_i|docIds) here for all terms in docIds; unnormalized
        switch(qGConf.method) {
            case "popular" :    return new ML(termFreqsGroup, sttf_group);
            case "random" :     return new Uniform(
                                        termFreqsGroup.keySet().toArray(
                                        new String[termFreqsGroup.size()]));
            case "discriminative" : return new Discriminative(
                                            termFreqsGroup.keySet().toArray(
                                                    new String[termFreqsGroup.size()]),
                                            lexicon);
            default:            throw new RuntimeException("invalid value for --method");
        }
    }

    private static void getTermFreqsGroup(
            String groupId,
            List<String> docIds, // used read-only
            IndexReader reader,
            IndexSearcher searcher,
            Map<String, Long> termFreqs // modified (elements inserted)
    ) throws IOException {
        for (String docId: docIds) {
            // for each term: 
            // Note: search() throws IOException
            TopDocs hits = searcher.search(new TermQuery(new Term("id", docId)), 1);
            ScoreDoc[] scoreDocs = hits.scoreDocs;
            if (scoreDocs.length == 0) {
                System.err.println("Notice: "
                        + "docId '" + docId
                        + "' from group '" + groupId 
                        + "' does not appear in the index");
                continue;
            }
            int docid = scoreDocs[0].doc;
            Terms terms = reader.getTermVector(docid, "content");
            if (null == terms) {
                continue;
            }
            TermsEnum termsEnum = terms.iterator(null);
            BytesRef binTerm;
            while (null != (binTerm = termsEnum.next())) {
                String term = binTerm.utf8ToString();

                //   - get the frequency of the term in the tweet, add this
                //   to the frequency of the term for h.
                long tfDoc = termsEnum.totalTermFreq(); // total term freq document
                //   - if you did not see this term before, get the
                //   frequency of the term in the entire corpus.
                if (termFreqs.containsKey(term)) {
                    termFreqs.put(term, termFreqs.get(term) + tfDoc);
                } else {
                    termFreqs.put(term, tfDoc);
                }
            }
        }
    }

    private static String sampleTerm(
            LM corpusLM,
            LM groupLM,
            Random random,
            Map<String, List<String>> groups, // used read-only
            IndexReader reader, // used read-only
            QGConf qGConf // used read-only
        ) throws IOException {
        // can't try forever, but as we are sampling with replacement, 
        // the same terms can come back, and we have to give it some
        // time. But in practice, this will likely take only very
        // few iterations. We'll take at most 10000 iterations.
        for (int i = 0; i < 10000 * 3; ++i) {
            String candidateTerm = sampleCandidateTerm(corpusLM, groupLM, random, qGConf);
            // Note, acceptTerm throws IOException
            if (acceptTerm(candidateTerm, groups, reader, qGConf)) {
                return candidateTerm;
            }
        }
        return ""; // no good terms apparent, although in theory if we kept on
                   // sampling we might still find one :-)
    }

    private static boolean acceptTerm(
            String term, // used read-only
            Map<String, List<String>> groups, // used read-only
            IndexReader reader, // used read-only
            QGConf qGConf // used read-only
            ) throws IOException {
        // In this implementation of Azzopardi et al, 2007, we allow some criteria to
        // reject a sampled query term.
        //
        // disregard term if it equals any of the hashtags up to the leading '#' character.
        if (groups.containsKey(term.replaceFirst("^#", ""))) {
            return false;
        }
        // disregard term if docFreq is lower than minDocFreq
        //  Note: docFreq() throws IOException
        if (reader.docFreq(new Term("content", term)) < qGConf.minDocFreq) {
            return false;
        }
        return true;
    }

    private static String sampleCandidateTerm(
            LM corpusLM,
            LM groupLM,
            Random random,
            QGConf qGConf
        ) {
        // Draw a uniform random number from [0, 1), and sample either
        // from background model or from group model. Remember from the
        // Azzopardi paper that ultimately we sample from a linear interpolation:
        //
        //      (1 - lambda) * p(t_i|group) + lambda * p(t_i)
        //
        // We don't explicitly calculate this sum, but instead sample from
        // either p(t_i|group) with probability (1 - lambda), or from
        // p(t_i) with probability lambda. The probability of sampling a term
        // then exactly equals the above sum. This is much more efficient
        // to implement then first summing and then sampling from the always
        // dense distribution.
        double p_which = random.nextDouble(); // [0, 1)
        if (p_which < qGConf.lambda ) {
            // sample from background corpus
            return corpusLM.sample(random);
        } else {
            // sample from group corpus
            return groupLM.sample(random);
        }
    }

    /**
     * sample a query length from a Poisson distribution with avg avgLength.
     *
     * Depends on Apache Commons Math library.
     */
    public static int sampleQueryLength(double avgLength,
            RandomDataGenerator rdg) {

        // round to the nearest integer, as described in Azzopardi et al, 2007
        // [1]. 
        if (avgLength < 0.0d) {
            throw new IllegalArgumentException("Please supply avgLength > 0");
        }
        long avg = Math.round(avgLength);

        // sample nextPoisson
        long next = rdg.nextPoisson(avg);
        while (next < Integer.MIN_VALUE || next > Integer.MAX_VALUE) {
            next = rdg.nextPoisson(avg);
        }
        return (int)next;
    }  
}

class QGConf {
    public final String method; 
    public final double avgQueryLength;
    public final boolean verbose;
    public final String indexDir;
    public final PrintStream out;
    public final long seedQueries;
    public final long seedTerms;
    public final double lambda;
    public final int minDocFreq;

    public QGConf(String method, double avgQueryLength,
            boolean verbose, String indexDir,
            PrintStream out, long seedQueries,
            long seedTerms, double lambda,
            int minDocFreq) {
        this.method = method;
        this.avgQueryLength = avgQueryLength;
        this.verbose = verbose;
        this.indexDir = indexDir;
        this.out = out; 
        this.seedQueries = seedQueries;
        this.seedTerms = seedTerms;
        this.lambda = lambda;
        this.minDocFreq = minDocFreq;
    }

    public void printVerbose(String msg) {
        if (verbose) {
            System.out.print(msg);
        }
    }
}
