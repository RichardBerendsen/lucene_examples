package nl.richardberendsen.lucene.querygeneration;

import java.util.Random;

/**
 * LM is a language model interface. Implementing classes should support
 * sampling a term from this model; note that this may be a pointer to a string
 * in the language model to avoid duplicate memory allocation.
 *
 * Interfaces should also provide the option to seed the sampling.
 *
 */
public interface LM {
        String sample(Random random);
}
