package nl.richardberendsen.lucene.querygeneration;

import java.util.Random;
import java.util.Map;

import nl.richardberendsen.lucene.querygeneration.LM;

public class ML implements LM {

    String[] maxLikelihoodLM;

    @Override
    public String sample(Random random) {
        // random.nextInt(x) samples from [0, x)
        return maxLikelihoodLM[random.nextInt(maxLikelihoodLM.length)];
    }

    public ML(Map<String, Long> termFrequencies, long sttf) {
        buildLM(termFrequencies, sttf);
    }

    private void buildLM(Map<String, Long> termFrequencies, long sttfLong) {
        // as this class tries an in-memory approach, we check if sttf
        // can be cast as an integer, and then try to allocate an array
        // of sttf String pointers.
        if (sttfLong < Integer.MIN_VALUE || sttfLong  > Integer.MAX_VALUE) {
            throw new RuntimeException("sttf cannot be cast as integer");
        }
        int sttf = (int) sttfLong;

        // try to fit sttf in memory, hoping we'll get an OutOfMemoryError if
        // it does not fit.
        maxLikelihoodLM = new String[sttf]; 

        // walk over termFrequencies, putting pointers to terms in there in LM.
        int start = 0;
        for (Map.Entry<String, Long> termFrequency: termFrequencies.entrySet()) {
            String term = termFrequency.getKey();
            // note that since sttf < Integer.MAX_VALUE, so must freq
            int freq = termFrequency.getValue().intValue();
            int end = start + freq; 
            for (int i = start; i < end; ++i) {
                maxLikelihoodLM[i] = term; 
            }
            start += freq;
        }
    }

}
