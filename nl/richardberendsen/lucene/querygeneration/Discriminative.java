package nl.richardberendsen.lucene.querygeneration;

import java.util.List;
import java.util.ArrayList;
import java.util.Map;
import java.util.SortedMap;
import java.util.TreeMap;
import java.util.Random;

import nl.richardberendsen.lucene.querygeneration.LM;

import nl.richardberendsen.math.Interval;


public class Discriminative implements LM {

    List<SortedMap<Interval, String>> partition;

    @Override
    public String sample(Random random) {
        // random.nextDouble() samples from [0, 1)
        double rScaled = random.nextDouble() * partition.size();
        // rScaled is in [0, partition.length)
        // and partition.length must fit in an integer since it is an array
        // length.
        int rScaledFloored = (int) Math.floor(rScaled);
        return partition.get(rScaledFloored).get(
                new Interval(rScaled, rScaled));
    }

    public Discriminative(
            // vocabulary of foreground corpus, this class stores pointers from
            // vocabulary.
            String[] vocabulary,  
            // lexicon with term frequencies of background corpus, used
            // read-only
            Map<String, Long> lexicon) {
        buildLM(vocabulary, lexicon);
    }

    private void buildLM(
                String[] vocabulary, // used read-only
                Map<String, Long> lexicon // used read-only
                ) {
        // idea: we calculate p(t_i|group) for t_i in vocubulary
        // and make a partition array for quick lookup: stopword 
        // terms have low probabilities, and multiple of these
        // terms may end up in the same index of partition.
        // The bigger the partition, the less terms fall into
        // the same index. If the partition has the length of
        // the vocabulary, on average one term per index is present.
        Double[] P = new Double[vocabulary.length];
        for (int i = 0; i < vocabulary.length; ++i) {
            P[i] = 1.0d / lexicon.get(vocabulary[i]);
            // p is in (0, 1]
        }
        double mass = 0.0d;
        for (int i = 0; i < vocabulary.length; ++i) {
            mass += P[i];
        }
        // partition would range from [0, mass), so scale probabilities
        // such that they will fall in the range [0, vocabulary.length) 
        double scaleFactor = vocabulary.length / mass; 
        double start = 0.0d;
        // we know that the summed probabilities will add up to vocabulary.length
        // so we set the initialcapacity.
        partition = new ArrayList<SortedMap<Interval, String>>(vocabulary.length);
        for (int i = 0; i < vocabulary.length; ++i) {
            double p = scaleFactor * P[i];
            double end = start + p;
            // start and end are always in [0, vocabulary.length]
            // Even if we end up slightly above vocabulary.length due to
            // roundoff errors, we floor start, and we run only until
            // j < end, so array indexing will always be fine.
            for (int j = (int) Math.floor(start); j < end; ++j) {
                if (partition.size() == j) {
                    partition.add(new TreeMap<Interval, String>());
                } 
                partition.get(j).put(new Interval(start, end), vocabulary[i]);
            }
            start = end;
        }
    }
}
