# Example code using Lucene 4.6

Contains code to calculate log-likelihood ratio between term frequencies of terms in two corpora,
and code to do compound splitting using collection frequencies.
